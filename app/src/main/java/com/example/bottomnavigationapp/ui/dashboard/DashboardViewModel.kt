package com.example.bottomnavigationapp.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DashboardViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }

    val text: LiveData<String> = _text

    private val count:MutableLiveData<Int> by lazy{
        MutableLiveData<Int>()
    }
    fun initCount(){
        count.value = 0
    }
    fun plus(){
        count.value = count.value!!.plus(1)
    }
    fun count():MutableLiveData<Int>{
        return count

    }

}
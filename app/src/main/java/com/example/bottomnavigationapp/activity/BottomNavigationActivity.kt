package com.example.bottomnavigationapp.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.bottomnavigationapp.R
import com.example.bottomnavigationapp.ViewPagerAdapter
import com.example.bottomnavigationapp.ui.dashboard.DashboardFragment
import com.example.bottomnavigationapp.ui.home.HomeFragment
import com.example.bottomnavigationapp.ui.notifications.NotificationsFragment
import kotlinx.android.synthetic.main.bottom_navigation_activity.*

class BottomNavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bottom_navigation_activity)
        init()

    }

    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(HomeFragment())
        fragments.add(DashboardFragment())
        fragments.add(NotificationsFragment())
        viewPager.adapter = ViewPagerAdapter(
            supportFragmentManager,
            fragments
        )
        clickOnNavigationItemSelected()

    }


    private fun clickOnNavigationItemSelected() {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                nav_view.menu.getItem(position).isChecked = true

            }

        })

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> viewPager.currentItem = 0
                R.id.navigation_dashboard -> viewPager.currentItem = 1
                R.id.navigation_notifications -> viewPager.currentItem = 2
            }
            true
        }
    }
}
